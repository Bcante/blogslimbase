
<?php  
	if (!empty($flash['info'])) // Message asynchrone, généré sur une page et délivré sur une autre
		// mécanisme slim : écrire ce message vide la variable correspondante
	   echo <<<YOP
	<div class="info">
	    {$flash['info']} 
	</div>
YOP
	?>
	
<!DOCTYPE html>
<html lang="fr">
    <head >
	<title>Mon super titre</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="scale=1.0" />
	<link rel="stylesheet" href="<?php echo $app->urlFor('root'); ?>/static/style.css" type="text/css" media="screen" />
    </head>
    <body>

	<section>
	    <div>
		Retour à la <a href="<?php echo $app->urlFor('root');?>">racine</a>
	    </div>
	</section>

	<div class ="logDelog">
		Se <a href="<?php echo $app->urlFor("deconnexion");?>">tirer d'ici </a>
	</div>
	
	<div class = "settings">
		Changer ses <a href="<?php echo $app->urlFor("formsettings");?>"> machins </a>
	</div>
	<hr>

