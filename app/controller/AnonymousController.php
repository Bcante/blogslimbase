<?php
use conf\Authentication;
use app\model\Utilisateurs;
require('vendor/autoload.php');
// Avec ce require je ne suis pas sensé avoir de problèmes pour avoir les modèles...

class AnonymousController extends Controller {

    public static function header() {
		$app = Controller::$app;
		/** Pour le moment on ne teste que si il y a une var de sesssion, dans le futur il faut tester la valeur de credential **/
		if (!isset($_COOKIE['idUser'])){
				$app->render('anonHeader.php',compact('app'));
			}
		else if ($_SESSION['lvlAcces'] === 0) {
			$app->render('userHeader.php',compact('app','utilisateurs')); // Il faut imaginer que utilisataurs et app sont précédés d'un $. On pourra l'utiliser dans la vue.
		}
		else {
			$app->render('adminHeader.php',compact('app'));
		}
	}

    public function footer() {
		if (isset($_COOKIE['idUser'])) {
			echo "bonjour ".$_SESSION['pseudo'];
			if ($_SESSION['lvlAcces']===1) {
				Controller::$app->render('adminFooter.php');
			}
		Controller::$app->render('footer.php');
		}
    }

    public function index(){
		// Vérifiation si on a rien dans les var de session, et un cookie en attente.
		$this->verifLog();
		
		$this->header();
		Controller::$app->render('ligneAddBillet.php');
		$tousBillets = Billets::orderBy('date','DESC')->get();
		// Le plus récent doit être en haut
		Controller::$app->render('homepage.php', compact('tousBillets'));
		$this->footer();
    }
    

/** C'est par ici qu'on rentre pour entrer une donnée. Ce controler va déclencher arf.php, qui contient 
 * Lui même un formulaire ayant pour action ajout_info! ajout_info étant une route dans le yaml, qui renvoie...
 * vers insert_info. C'est donc normal que ça soit pas accessible at first sight **/
    public function yopla(){
		$this->header();
		Controller::$app->render('arf.php');
		$this->footer();
    }

    public function affiche_item($id){ // Il faudrait passer non pas l'id, mais l'objet en entier.
		$this->header();
		Controller::$app->render('aff_item.php', compact('id'));
		$this->footer();
    }

    public function insert_info(){
		$app = Controller::$app;
		// Ne pas oublier le filtrage de donné, les vérifications... (!!!)
		$nom = $app->request->post('nom'); // Post empêche d'utiliser des paramètres. Donc là comme ça j'accèdre directement au post.
		$app->flash('info', "J'ai ajouté le nom « $nom »"); // Catégorie de flash, message
		$app->redirectTo('root'); // C'est cette redirection qui va afficher les message
		// slim facilite le travail
    }
    
    public function affiche_billet($id) {
		$this->header();
		$billetCible = Billets::find($id);
		Controller::$app->render('billetCible.php', compact('billetCible'));
		$this->footer();
		if (isset($_SESSION['pseudo'])) {
			setcookie("dernierePage", $id ,time() + 60*60*24*7); 
			Controller::$app->render('addCommentaire.php');
		}
	}
    
    /** Affiche le formulaire d'inscription ainsi que le header de la page.
     * */
    public function inscritMembre() {
		$this->header();
		Controller::$app->render('form_insc.php');
	}
	
	public function solidInscription() {
		/** Faut il envoyer chier l'utilisateur qui met des balises? **/
		$app = Controller::$app;
		$formulaireOk = true; // Dès qu'un des champs est mal fait on désactive ce boolean
		
		$nom = $app->request->post('nom');
			$nom = filter_var($nom, FILTER_SANITIZE_STRING);
		
		$prenom = $app->request->post('prenom');
			$prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
		
		$mail = $app->request->post('mail');
			filter_var($mail, FILTER_VALIDATE_EMAIL);
			$mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
			
		// c'est pas un peu tard pour encrypter le mdp?
		
		$mdp = $app->request->post('mdp');
			// Le filtre risque pas de niquer tout?
		$mdp = filter_var($mdp, FILTER_SANITIZE_STRING); // Cette ligne va hasher le mot de passe. 
		$mdphash = password_hash($mdp, PASSWORD_BCRYPT);
		
		$pseudo = $app->request->post('pseudo');
			$pseudo = filter_var($pseudo, FILTER_SANITIZE_STRING);

		if ($formulaireOk === true) {
			$app->flash('info', "Vous êtes maintenant inscris! "); 
			
			$e = new Utilisateurs();
			$e->pseudo = $pseudo;
			$e->mdp = $mdphash;
			$e->nom = $nom;
			$e->prenom = $prenom;
			$e->mail = $mail;
			
			$e->save();
			
			$app->redirectTo('formconnexion'); // Utilisation d'un redirect plutôt qu'une fonction pour que le flash apparaisse
		}
		else {
			$app->flash('info', "Vous avez fais le malin?"); 
			$app->redirectTo('root'); 
		}
		
			
		// Ne pas oublier le filtrage de donné, les vérifications... (!!!)
		//$nom = $app->request->post('nom'); // Post empêche d'utiliser des paramètres. Donc là comme ça j'accèdre directement au post.
		//$app->flash('info', "J'ai ajouté le nom « $nom »"); 
		//C'est le champ name du form qui est pris en compte, pas l'id.
		// A faire : Saler le mot de passe et l'enregistrer dans la base de donnée
	}
		
	public function connecteMembre() {
		// N'affiche le form de connexion que si je ne suis pas connecté
		$this->header();
		Controller::$app->render('form_connexion.php');
	}
	
	public function solidConnexion() {
		$app = Controller::$app;
		
		$pseudo = $app->request->post('pseudo');
			$pseudo = filter_var($pseudo, FILTER_SANITIZE_STRING);
		
		$mdp = $app->request->post('mdp');
			$mdp = filter_var($mdp, FILTER_SANITIZE_STRING);
		
		/** On prend les informations sur les utilisateurs de la BDD pour voir si on trouve un utilisateur
		 * ayant le même pseudo que celui qui veut se connecter **/
		$e = new Utilisateurs();
		$allUser = Utilisateurs::all();
		$mdpHash = "";
		
		/** TODO amélioration de la boucle (si 1k utilisateurs..)**/
		foreach ($allUser as $usrTmp) {
			if ($usrTmp->pseudo === $pseudo) {
				$usrMatch = $usrTmp;
				$mdpHash = $usrMatch->mdp;
			}
			// On isole le hash de la personne qui nous intéresse...
		}
		
		$connexionOK = Authentication::authenticate($mdp, $mdpHash);

		/** Connexion en dure: On attribue les informations de la BDD à l'user. 
		 * TODO positionner un cookie **/
		 
		if ($connexionOK) { // Positionnement var session & cookie
			session_destroy(); // session_regenerate_id ?
			session_start();
			$profil = $usrMatch->profil;
			// On détruit tout ce qu'il y a dedans et on recommence
	
			//Ces informations doivent être soigneusement salées etc ...
			$idUsr = $usrMatch->id;
			$idUsrHash = password_hash($idUsr, PASSWORD_BCRYPT);
			setcookie("idUser", $idUsrHash ,time() + 60*60*24*7); // Servira de 'clef' pour accèder aux variables de sessions
			if ($profil === "admin") {
				$_SESSION['lvlAcces'] = 1; // le lvl d'accès de la BDD,  membre 1 admin
			}
			else $_SESSION['lvlAcces'] = 0;
			$_SESSION['pseudo'] = $pseudo; 
			
			$_SESSION['id'] = $idUsr;
			
			$app->flash('info', "Bonjour !"); 
			$app->redirectTo('root');
		}
		
		else {
			$app->flash('info', "Information de connexion erronées.<br>"); 
			$app->redirectTo('formconnexion');
		}
			
	}
	
	public function verifLog() {
		// Vérifiation si on a rien dans les var de session, et un cookie en attente.
		if (isset($_COOKIE['idUser']) && !isset($_SESSION['pseudo']) ) {
			$usrList = Utilisateurs::all();
			foreach ($usrList as $usrTmp) {
				if (password_verify($usrTmp->id, $_COOKIE['idUser'])) {
					if ("admin" === $usrTmp->profil) $_SESSION['lvlAcces'] = 1;
					else $_SESSION['lvlAcces'] = 0;
		
					$_SESSION['pseudo'] = $usrTmp->pseudo;
					$_SESSION['id'] = $usrTmp->id;
				}
				//. On vérifie user par user si j'ai un id qui correspond. 
				// Une fois trouve, je met dans ma var de session les valeurs de cet user
			}
		}
	}
	
}

?>
