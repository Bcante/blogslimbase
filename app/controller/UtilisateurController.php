<?php
use conf\Authentication;

// Pas de session start?
class UtilisateurController extends Controller {
	 // Deco
	 public function deconnexion() {
		$app = Controller::$app;
		Authentication::logout();
		$app->flash('info', "Aurevoir.");
		$app->redirectTo('root');
	 } 
	 
	 // commenter
	/** public function formComment() {
		 Controller::$app->render('addCommentaire.php');
	 }**/
	 
	 public function solidComment() {
		$app = Controller::$app;
		$commentaire = $app->request->post('commentaire');
		$commentaire = filter_var($commentaire, FILTER_SANITIZE_STRING);
		
		// On s'assure que l'utilisateur n'a rien fait de malhonnête dans le cookie
		$idBillet = filter_var($_COOKIE['dernierePage'],FILTER_SANITIZE_NUMBER_INT);
		setcookie('idUser',null);
		$idUsr = $_SESSION['id'];
		
		$nvCommentaire =new Commentaires();
		$nvCommentaire->message=$commentaire;
		$nvCommentaire->id_billet=$idBillet;
		$nvCommentaire->id_utilisateur=$idUsr;
		$nvCommentaire->save();
		$app->redirectTo('root');
	 }
	 
	 // saisir un billet
	 public function formBillet() {
		 AnonymousController::header();
		 $categories = Categories::all();
		 $labels = array();
		 foreach ($categories as $catTmp) {
			 array_push($labels,$catTmp->label);
		 }
		 Controller::$app->render('redacBillet.php',compact('labels'));
	 }
	 
	 public function solidBillet() {
		AnonymousController::header();
		$app = Controller::$app;
		$billet = $app->request->post('billet');
		$billet = filter_var($billet, FILTER_SANITIZE_STRING);
		$titre = $app->request->post('titre');
		$titre = filter_var($titre, FILTER_SANITIZE_STRING);
		
		$nvBillet = new Billets();
		$nvBillet->message=$billet;
		//pas touche à la date, car timestamp
		$nvBillet->titre=$titre;
		/** Valeurs en dur à changer par la magie des cookies / post **/
		$nvBillet->id_utilisateur = $_SESSION['id'];
		$nvBillet->id_categorie = $app->request->post('selectCategorie');
		$nvBillet->save();
		
		
		//$app->redirectTo('root');
	 }
	 
	 public function formSettings() {
		AnonymousController::header();
		Controller::$app->render('changesettings.php');
	 }
	 
	 public function solidSettings() {
		$changement = false;
		
		$app = Controller::$app;
		// Vérification: Pour voir si rien n'a été changé (active un boolean)
		$allPostVars = $app->request->post();
		$utilisateur = Utilisateurs::find($_SESSION['id']);
		
		foreach($allPostVars as $cle => $element) {
			if ($element !== "" && $element  !== "Go go go!") { // Il faut virer le gogogo qui compte comme un élément...
				// svg de $element dans une de mes variables..
				// On doit virer le gogo qui correspond au bouton...
				$changement = true;
				 switch ($cle) {
					 // nicht fergessen sanitize
					 case "newName":
						$newName = filter_var($element, FILTER_SANITIZE_STRING);
						$utilisateur->nom = $element;
						break;
					case "newFirstName":
						$element = filter_var($element, FILTER_SANITIZE_STRING);
						$utilisateur->prenom = $element;						
						break;
					case "newMail":
						if (filter_var($element, FILTER_VALIDATE_EMAIL)) { // supprime l'email
							$element = filter_var($element, FILTER_SANITIZE_EMAIL);
						}
						$utilisateur->mail = $element;
						break;
					case "newMdp":
						$element = filter_var($element, FILTER_SANITIZE_STRING);
						$utilisateur->mdp = password_hash($element, PASSWORD_BCRYPT);	
						break;
				 }
						$utilisateur->save();
			}
		}
		
		if (!$changement) {
			$app->redirectTo('root');
		}
		else {
			$this->deconnexion();
		}
		// Que faire si l'un de ses champs n'est pas corrects
		// envoie bdd
	 }
}
