<?php
use conf\Authentication;
/** Réservé aux meilleurs des meilleurs
 * Peut avoir accès à la liste des membres (et les radier)
 * Ajouter des catégories de blog
 * */
class AdminController extends Controller {
	public function afficheMembres() {	
		// Pour chaqun des membres, on affiche son pseudo et une croix à côté...
		// Il faut donc avoir un array contenant la liste de tous les membres, pour chaqun d'eux mettre une croix?
		// Chaqun de ces boutons permet de déclencher la supression d'un et un seul membre
		// ça devrait déclencher une méthode post.
		// même logique que pour les catégories, soit on passe des data à cette vue (mais comment je sais pas) soit on fait ça salement.
		Controller::$app->render('listeMembre.php');
	}
	
	public function solidRadiation(){
		// Permet la radiation effective d'un membre
		// Comment retrouver la valeur du bouton qui a envoyé l'ordre
		$app = Controller::$app;
		$heureuxGagnant = $app->request->post('idMembre');
		AnonymousController::header();
		echo $heureuxGagnant;
		
	}
	
	public function formCategorie() {
		AnonymousController::header();
		Controller::$app->render('ajouteCategorie.php');
	}
	
	public function solidCategorie() {
		$app = Controller::$app;
		AnonymousController::header();
		$nouvelleCat = $app->request->post('nvlCategorie');
		$nvlCat = new Categories();
		$nvlCat->label = $nouvelleCat;
		
		// Vérification d'un potentiel doublon
		$allCategories = Categories::all();
		$doublon = false;
		foreach ($allCategories as $cat) {
			if ($cat->label == $nouvelleCat) {
				$doublon = true;
			}
		}
		
		if (!$doublon) {
			$app->flash('info', "Ajout de la nouvelle catégorie '$nouvelleCat'<br>"); 
			$nvlCat->save();
		}
		else $app->flash('info', "Cette catégorie existe déjà! <br>");
		$app->redirectTo('root');
		
	}
		
}
