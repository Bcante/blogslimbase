Consignes de configuration du serveur Web Apache
================================================

**Attention** : si vous travaillez sur votre machine personnelle, vérifiez
la configuration de votre serveur Web pour que les options exprimées
dans le fichier .htaccess puissent être prises en compte. Elles
permettent le mécanisme de réécriture, essentiel pour le
fonctionnement de Slim.

Sous une distribution Debian/Ubuntu, il faut éditer (avec les droits
du super-utilisateur) le fichier `/etc/apache2/apache2.conf`

À l'intérieur de ce fichier, il y a en principe par défaut une
section :

	<Directory /var/www/>
	        Options Indexes FollowSymLinks
	        AllowOverride None
	        Require all granted
	</Directory>

Il s'agit de remplacer la valeur associée à la directive AllowOverride
pour lui associer la valeur « All ».

    <Directory /var/www/>
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
    </Directory>

Une fois la modification effectuée, il faut enregistrer et, toujours
avec les droits du super-utilisateur, taper :

`service apache2 restart`