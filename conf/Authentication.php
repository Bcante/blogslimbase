<?php

namespace conf;
use app\model\Utilisateurs;

//require('vendor/autoload.php');

class Authentication {

    public static function authenticate($passUser, $passHash) {
		$res = false;
		
		if (password_verify($passUser, $passHash)) {
			$res = true;
		}
		return $res;
    }


    public static function logout() {
		$allUser = Utilisateurs::all();
		$speUser = Utilisateurs::find(1);
		session_destroy();
		setcookie('idUser',null);
		/** Pour tous les cookies crées : setCookie('nom',null);**/
		
		session_start(); // On recommence une session à zéro. 
    }

    public static function isAuthenticated() {
    }

    public static function getUser() {
	// More code needed here
    }

    // More methods needed here
}
